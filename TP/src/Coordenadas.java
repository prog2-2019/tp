public class Coordenadas<X, Y> {

	private int x, y;

	public Coordenadas(int a, int b) {
		this.x = a;
		this.y = b;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	@Override
	public String toString() {
		return ("[" + this.x + ", " + this.y + "]");
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Coordenadas<X, Y> other = (Coordenadas<X, Y>) obj;
		if (this.x != other.getX())
			return false;
		if (this.y != other.getY())
			return false;
		return true;
	}
}
