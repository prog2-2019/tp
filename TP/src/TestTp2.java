import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class TestTp2 {

	private GR gr1, gr2, gr3;

	@Before
	public void setUp() throws Exception {
		gr1 = new GR(11, 10);
		gr2 = new GR(11, 11);
		gr3 = new GR(11, 10);
	}

	@Test
	public void testMismasAreasMismasJugadas() {
		gr1.jugar(3, 2);
		gr1.jugar(2, 6);
		gr2.jugar(3, 2);
		gr2.jugar(2, 6);
		assertEquals(gr1.area(1), gr2.area(1));
		assertTrue(!gr1.equals(gr2)); // no son iguales porque gr1 y gr2 son de distinto tama�o
		gr3.jugar(3, 2);
		gr3.jugar(2, 6);
		assertEquals(gr1.area(1), gr3.area(1));
		assertTrue(gr1.equals(gr3));
	}

	@Test
	public void testMismasAreasDiferentesJugadas() {
		gr1.jugar(3, 2);
		gr1.jugar(1, 1);
		gr1.jugar(2, 6);
		gr2.jugar(2, 6);
		gr2.jugar(1, 1);
		gr2.jugar(3, 2);
		assertEquals(gr1.area(1), gr2.area(1));
		gr3.jugar(2, 6);
		gr3.jugar(1, 1);
		gr3.jugar(3, 2);
		assertEquals(gr1.area(1), gr3.area(1));
		assertTrue(!gr2.equals(gr3)); // no son iguales porque gr2 y gr1 son de distinto tama�o
		assertTrue(!gr1.equals(gr3)); // no son iguales porque los rectangulos son de diferentes jugadores
	}

	@Test
	public void testQuitarRectangulos() {
		gr1.jugar(3, 2);
		gr1.jugar(3, 2);
		assertEquals(gr1.area(1), gr1.area(2));
		gr1.eliminarRect();
		assertNotEquals(gr1.area(1), gr1.area(2));
		gr2.jugar(3, 2);
		gr2.eliminarRect();
		assertEquals(gr2.area(1), 0);
		gr3.eliminarRect();
		assertEquals(gr2.area(1), 0);
	}

}
