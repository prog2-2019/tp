import java.util.ArrayList;
import java.util.Iterator;

public class Conjunto<T extends Comparable<T>> {

	private ArrayList<T> datos;
	private T maximo, minimo;

	public Conjunto() {
		this.datos = new ArrayList<T>();
	}

	public int tamano() {
		return this.datos.size();
	}

	public void agregar(T elem) {
		if (!this.pertenece(elem)) {
			if (this.tamano() == 0) { // soy el primer elemento!
				this.maximo = elem;
				this.minimo = elem;
			} else {
				if (elem.compareTo(this.maximo) == 1) { // compareTo() devuelve
														// 1 si es mayor
					this.maximo = elem;
				}
				if (elem.compareTo(this.minimo) == -1) { // compareTo() devuelve
															// -1 si es menor
					this.minimo = elem;
				}
			}
			this.datos.add(elem);
		}
	}

	public void union(Conjunto<T> c) {
		for (int i = 0; i < c.tamano(); i++) {
			this.agregar(c.getIesimo(i)); // tipo T
		}
	}

	public Conjunto<T> union2(Conjunto<T> c) {
		Conjunto<T> nuevo = new Conjunto<T>();
		for (int i = 0; i < this.tamano(); i++) {
			nuevo.agregar(this.getIesimo(i));
		}
		for (int i = 0; i < c.tamano(); i++) {
			nuevo.agregar(c.getIesimo(i));
		}
		return nuevo;
	}

	// elimino los elementos que no esten en ambos!
	public void interseccion(Conjunto<T> c) {
		ArrayList<T> aux = new ArrayList<T>();
		for (int i = 0; i < c.tamano(); i++) {
			if (this.pertenece(c.getIesimo(i))) {
				aux.add(c.getIesimo(i)); // tipo T
			}
		}
		this.datos = aux;
	}

	public Conjunto<T> interseccion2(Conjunto<T> c) {
		Conjunto<T> nuevo = new Conjunto<T>();
		for (int i = 0; i < c.tamano(); i++) {
			if (this.pertenece(c.getIesimo(i))) {
				nuevo.agregar(c.getIesimo(i));
			}
		}
		return nuevo;
	}

	// elimino los elementos que no esten en ambos!
	public void interseccion3(Conjunto<T> c) {
		for (int i = this.tamano() - 1; i > 0; i--) {
			if (!c.pertenece(this.getIesimo(i))) {
				this.eliminarPorIndice(i); // eliminar por indice
			}
		}
	}

	// elimino los elementos que no esten en ambos!
	public void interseccion4(Conjunto<T> c) {
		for (int i = this.tamano() - 1; i > 0; i--) {
			if (!c.pertenece(this.getIesimo(i))) {
				this.eliminarPorElemento(this.getIesimo(i)); // eliminar por
																// elemento
			}
		}
	}

	public void eliminarPorIndice(int i) {
		this.datos.remove(i);
	}

	public void eliminarPorElemento(T elem) {
		for (int i = 0; i < this.tamano(); i++) {
			if (this.getIesimo(i).equals(elem)) {
				this.datos.remove(i);
			}
		}
	}

	public T getMaximo() {
		return this.maximo;
	}

	public T getMinimo() {
		return this.minimo;
	}

	public String toStringSinStringBuilder() {
		String ret = "";
		ret += "[";
		for (int i = 0; i < this.tamano() - 1; i++) {
			ret += this.datos.get(i).toString() + ", ";
		}
		ret += this.datos.get(this.tamano() - 1).toString() + "]";
		return ret;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		Iterator<T> it = this.datos.iterator();
		s.append("[");
		while (it.hasNext()) {
			s.append(it.next().toString());
			s.append(", ");
		}
		s.deleteCharAt(s.length() - 1);
		s.deleteCharAt(s.length() - 1);
		s.append("]");
		return s.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (this.getClass() != obj.getClass()) {
			ret = false;
		}
		if (ret) {
			Conjunto<T> other = (Conjunto<T>) obj;
			if (this.tamano() != other.tamano()) {
				ret = false;
			}
			for (int i = 0; i < this.tamano(); i++) {
				ret = ret && this.getIesimo(i).equals(other.getIesimo(i));
			}
		}
		return ret;
	}

	public boolean pertenece(T elem) {
		return this.datos.contains(elem);
	}

	public T getIesimo(int i) { // me da el elemento i de la coleccion
		return this.datos.get(i);
	}
}
