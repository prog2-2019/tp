public class Rectangulo implements Comparable<Rectangulo> {

	private Coordenadas<Integer, Integer> coordenadas; // representan las coordenadas donde esta el vertice superior
														// izquierdo del rectangulo
	private int ancho, alto;

	public Rectangulo(Coordenadas<Integer, Integer> coordenadas, int ancho, int alto) {
		if (coordenadas.getX() >= 0 || coordenadas.getY() >= 0) {
			this.coordenadas = coordenadas;
			this.ancho = ancho;
			this.alto = alto;
		} else {
			throw new RuntimeException("Las coordenadas deben ser positivas.");
		}
	}

	public int area() {
		return this.ancho * this.alto;
	}

	public int ancho() {
		return this.ancho;
	}

	public int alto() {
		return this.alto;
	}

	public Coordenadas<Integer, Integer> coordenadas() {
		return coordenadas;
	}

	@Override
	public String toString() {
		return "Coordenada x: " + this.coordenadas.getX() + ", coordenada y: " + this.coordenadas.getY() + ", ancho: "
				+ this.ancho + ", alto: " + this.alto;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Rectangulo other = (Rectangulo) obj;
		if (this.alto != other.alto())
			return false;
		if (this.ancho != other.ancho())
			return false;
		if (this.coordenadas == null) {
			if (other.coordenadas() != null)
				return false;
		} else if (!this.coordenadas.equals(other.coordenadas()))
			return false;
		return true;
	}

	@Override
	public int compareTo(Rectangulo other) {
		if (this.area() > other.area()) {
			return 1;
		}
		if (this.area() < other.area()) {
			return -1;
		}
		return 0;
	}
}
