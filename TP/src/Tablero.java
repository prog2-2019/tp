public class Tablero {

	private int[][] tablero;
	private Conjunto<Rectangulo> rectangulosJ1, rectangulosJ2;
	private Rectangulo ultimoRectangulo;

	public Tablero(int ancho, int alto) {
		if ((ancho > 1 && alto > 0) || (ancho > 0 && alto > 1)) {
			this.tablero = new int[alto][ancho];
			for (int i = 0; i < this.height(); i++) {
				for (int j = 0; j < this.width(); j++) {
					this.tablero[i][j] = 0; // el tablero comienza con ceros en todas sus posiciones
				}
			}
			this.rectangulosJ1 = new Conjunto<Rectangulo>();
			this.rectangulosJ2 = new Conjunto<Rectangulo>();
		} else {
			throw new RuntimeException("El tablero debe ser mayor a 1x1");
		}
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		for (int i = 0; i < this.height(); i++) {
			for (int j = 0; j < this.width(); j++) {
				ret.append(this.tablero[i][j] + " ");
			}
			ret.append("\n");
		}
		return ret.toString();
	}

	public Conjunto<Rectangulo> rectangulosJ1() {
		return this.rectangulosJ1;
	}

	public Conjunto<Rectangulo> rectangulosJ2() {
		return this.rectangulosJ2;
	}

	public Rectangulo ultimoRectangulo() {
		return this.ultimoRectangulo;
	}

	// agrego 1's o 2's al tablero dependiendo del jugadorID, agrego el rectangulo
	// al conjunto que corresponde y actualizo el ultimo rectangulo
	public void agregarRectangulo(Rectangulo rectangulo, int jugadorID) {
		for (int i = rectangulo.coordenadas().getY(); i < rectangulo.coordenadas().getY() + rectangulo.alto(); i++) {
			for (int j = rectangulo.coordenadas().getX(); j < rectangulo.coordenadas().getX()
					+ rectangulo.ancho(); j++) {
				if (jugadorID == 1 || jugadorID == 2) {
					this.tablero[i][j] = jugadorID;
				}
			}
		}
		if (jugadorID == 1) {
			this.rectangulosJ1.agregar(rectangulo);
		}
		if (jugadorID == 2) {
			this.rectangulosJ2.agregar(rectangulo);
		}
		this.ultimoRectangulo = rectangulo;
	}

	// antes de generar el rectangulo verifico si entra en el tablero y tambien a
	// que jugador pertenece dicho rectangulo, si no entra, arrojo excepcion
	public Rectangulo generarRectangulo(int ancho, int alto, int jugadorID) {
		if (jugadorID == 1) {
			for (int i = 0; i < this.height(); i++) {
				for (int j = 0; j < this.width(); j++) {
					if (entra(i, j, ancho, alto)) {
						Coordenadas<Integer, Integer> coordenadas = new Coordenadas<Integer, Integer>(i, j);
						return new Rectangulo(coordenadas, ancho, alto);
					}
				}
			}
		}
		if (jugadorID == 2) {
			for (int i = this.width() - 1; i >= 0; i--) {
				for (int j = this.height() - 1; j >= 0; j--) {
					if (entra(i, j, ancho, alto)) {
						Coordenadas<Integer, Integer> coordenadas = new Coordenadas<Integer, Integer>(i, j);
						return new Rectangulo(coordenadas, ancho, alto);
					}
				}
			}
		}
		throw new RuntimeException("El rectangulo no entra.");
	}

	public void quitarRectangulo(int key, int jugadorID) {
		if (jugadorID == 1) {
			if (this.rectangulosJ1.getIesimo(key) == null) {
				throw new RuntimeException("No hay ning�n rect�ngulo para eliminar.");
			}
			this.borrarDelTablero(key, this.rectangulosJ1);
			this.rectangulosJ1.eliminarPorIndice(key);
		}
		if (jugadorID == 2) {
			if (this.rectangulosJ2.getIesimo(key) == null) {
				throw new RuntimeException("No hay ning�n rect�ngulo para eliminar.");
			}
			this.borrarDelTablero(key, this.rectangulosJ2);
			this.rectangulosJ2.eliminarPorIndice(key);
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Tablero other = (Tablero) obj;
		if (this.rectangulosJ1 == null) {
			if (other.rectangulosJ1() != null)
				return false;
		} else if (!this.rectangulosJ1.equals(other.rectangulosJ1()))
			return false;
		if (this.rectangulosJ2 == null) {
			if (other.rectangulosJ2() != null)
				return false;
		} else if (!this.rectangulosJ2.equals(other.rectangulosJ2()))
			return false;
		if (this.height() != other.height() || this.width() != other.width())
			return false;
		else if (this.height() == other.height() && this.width() == other.width()) {
			boolean ret = true;
			for (int i = 0; i < this.height(); i++) {
				for (int j = 0; j < this.width(); j++) {
					ret = ret && this.tablero[i][j] == other.tablero[i][j];
				}
			}
			return ret;
		}
		if (this.ultimoRectangulo == null) {
			if (other.ultimoRectangulo() != null)
				return false;
		} else if (!this.ultimoRectangulo.equals(other.ultimoRectangulo()))
			return false;
		return true;
	}

	public int height() {
		return this.tablero.length;
	}

	public int width() {
		return this.tablero[0].length;
	}

	// verifico si el rectangulo que se intenta colocar esta dentro del rango del
	// tablero y ademas si no se solapa con otro rectangulo que este en el tablero
	private boolean entra(int x, int y, int ancho, int alto) {
		return ((y + alto) <= this.height()) && ((x + ancho) <= this.width()) && !seSolapa(x, y, ancho, alto);
	}

	// veo si en donde se intenta colocar el rectangulo ya hay algun rectangulo de
	// cualquiera de los dos jugadores
	private boolean seSolapa(int x, int y, int ancho, int alto) {
		boolean ret = false;
		for (int i = y; i < y + alto; i++) {
			for (int j = x; j < x + ancho; j++) {
				ret = ret || this.tablero[i][j] != 0;
			}
		}
		return ret;
	}

	// recorro el tablero a partir del conjunto de rectangulos que me dan con su
	// respectivo indice y agrego ceros a la matriz, recibe el conjunto para no
	// estar preguntando si es el conjunto de j1 o j2
	private void borrarDelTablero(int key, Conjunto<Rectangulo> conjunto) {
		for (int i = conjunto.getIesimo(key).coordenadas().getY(); i < conjunto.getIesimo(key).coordenadas().getY()
				+ conjunto.getIesimo(key).alto(); i++) {
			for (int j = conjunto.getIesimo(key).coordenadas().getX(); j < conjunto.getIesimo(key).coordenadas().getX()
					+ conjunto.getIesimo(key).ancho(); j++) {
				this.tablero[i][j] = 0;
			}
		}
	}

}
