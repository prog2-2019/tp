import java.util.concurrent.ThreadLocalRandom;

public class GR {

	private Tablero tablero;
	private int turno, turnosSalteados;
	private boolean termino;
	private String ganador;

	public GR(int ancho, int alto) {
		this.tablero = new Tablero(ancho, alto);
		this.turno = 1;
		this.turnosSalteados = 0;
		this.termino = false;
		this.ganador = "";
	}

	public String jugar() { // modo automatico (con dados)
		int dado1 = ThreadLocalRandom.current().nextInt(1, 7);
		int dado2 = ThreadLocalRandom.current().nextInt(1, 7);
		return this.jugar(dado1, dado2);
	}

	public String jugar(int ancho, int alto) { // modo semi-automatico
		int aux = this.turno;
		if (ancho > 0 && ancho < 7 && alto > 0 && alto < 7) {
			try { // intentamos agregar un rectangulo del ancho y alto dados como parametros
				if (aux == 1) {
					this.tablero.agregarRectangulo(this.tablero.generarRectangulo(ancho, alto, this.turno), this.turno);
					this.turnosSalteados = 0;
				}
				if (aux == 2) {
					this.tablero.agregarRectangulo(this.tablero.generarRectangulo(ancho, alto, this.turno), this.turno);
					this.turnosSalteados = 0;
				}
			} catch (RuntimeException e) { // si generarRectangulo nos arroja una excepcion, significa que el rectangulo
											// dado no entra en el tablero
				this.turnosSalteados++;
				System.out.println("El rectangulo de " + ancho + "x" + alto + " no entra. \n");
			}
			if (aux == 1) {
				this.turno = 2;
			}
			if (aux == 2) {
				this.turno = 1;
			}
			if (turnosSalteados == 2) {
				this.termino = true;
				this.ganador = this.darGanador();
			}
		}
		return this.ganador;
	}

	public Rectangulo ultimoRectangulo() {
		return this.tablero.ultimoRectangulo();
	}

	public int area(int jugadorID) { // seg�n el jugadorID que reciba la funcion, retorna la suma de todas las areas
										// que ocupan sus respectivos rectangulos
		int ret = 0;
		if (jugadorID == 1) {
			for (int i = 0; i < this.tablero.rectangulosJ1().tamano(); i++) {
				ret += this.tablero.rectangulosJ1().getIesimo(i).area();
			}
		}
		if (jugadorID == 2) {
			for (int i = 0; i < this.tablero.rectangulosJ2().tamano(); i++) {
				ret += this.tablero.rectangulosJ2().getIesimo(i).area();
			}
		}
		return ret;
	}

	public void eliminarRect() {
		int aux = this.turno;
		try { // intentamos eliminar un rectangulo al azar
			if (aux == 1) {
				int key = ThreadLocalRandom.current().nextInt(0, this.tablero.rectangulosJ2().tamano());
				this.tablero.quitarRectangulo(key, 2);
				this.turnosSalteados = 0;
			}
			if (aux == 2) {
				int key = ThreadLocalRandom.current().nextInt(0, this.tablero.rectangulosJ1().tamano());
				this.tablero.quitarRectangulo(key, 1);
				this.turnosSalteados = 0;
			}
		} catch (RuntimeException e) { // si quitarRectangulo arroja una excepcion significa que no hay ningun
										// rectangulo para eliminar
			System.out.println("No hay ning�n rect�ngulo para eliminar.");
		}
		if (aux == 1) {
			this.turno = 2;
		}
		if (aux == 2) {
			this.turno = 1;
		}
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append(this.tablero.toString() + "\n");
		ret.append("Ultimo rectangulo: " + this.ultimoRectangulo() + "\n");
		ret.append("Es el turno de: Jugador " + this.turno + "\n");
		if (termino) {
			ret.append(this.ganador);
		}
		return ret.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		GR other = (GR) obj;
		if (this.ganador == null) {
			if (other.ganador != null)
				return false;
		} else if (!this.ganador.equals(other.ganador))
			return false;
		if (this.tablero == null) {
			if (other.tablero != null)
				return false;
		} else if (!this.tablero.equals(other.tablero))
			return false;
		if (this.termino != other.termino)
			return false;
		if (this.turno != other.turno)
			return false;
		if (this.turnosSalteados != other.turnosSalteados)
			return false;
		return true;
	}

	// si el juego termin�, retorno el ganador segun el area de cada jugador
	private String darGanador() {
		if (termino) {
			if (this.area(1) == this.area(2)) {
				return "Empate.";
			}
			if (this.area(1) > this.area(2)) {
				return "Jugador 1 ha ganado.";
			} else {
				return "Jugador 2 ha ganado.";
			}
		}
		return "";
	}
}
